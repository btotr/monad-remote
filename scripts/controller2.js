function Controller() {
    var self = this;
    this.view = new View();
    /*new SoundPairing(function(pairingId){
       self.pairingId = pairingId;
       console.log("pairingId, ", pairingId)
       self.init();
    });*/
    
    self.pairingId = 123;
    self.init();
}

Controller.prototype.init = function init(){
    this.hostName = "frightening-mountain-1035.herokuapp.com";
    this.applianceId = Math.random().toString(36).substring(7);
    this.profileId = "Profile_" + this.applianceId;
    
    var ws = new WebSocket("ws://" + this.hostName + "/negotiator/" + this.pairingId);
    
    var self = this;
    
    // TODO get from avatar or some other preference
    var color = "#" + (Math.random().toString(16) + '000000').slice(2, 8);

    this.profile = { "id": this.profileId, "color": color};

    ws.onopen = function() {
        //this.sendNotification("appliance-connected", "%22websocketserver%22%3A%22ws%3A%2F%2F" + this.hostName + "%2Fnegotiator%2F" + this.pairingId + "%22");
        this.sendNotification("appliance-connected",  encodeURIComponent('"id":1, "hub":"'+navigator.userAgent+'", "name":"identifier", "websocket":"ws://'+this.hostName+'/negotiator/'+ this.pairingId+'"'));
        setTimeout(function() {
            ws.send('{"event":"profileEnter", "profile": ' + JSON.stringify(self.profile) + ' }');
        }, 2000);
    }.bind(this);
    ws.onmessage = function(event) {
        console.log(event.data);
    };
    
    // enable dpad control
    this.dpad();
    
    window.addEventListener("beforeunload", function(){
        ws.send('{"event":"profileExit", "profile": ' + JSON.stringify(self.profile) + ' }');
        this.sendNotification("webkitgamepaddisconnected", encodeURIComponent('"gamepad":{"id":"cloudtv dpad control"}'));
        this.sendNotification("appliance-disconnected", encodeURIComponent('"id":1'));
    }.bind(this),false);
}

Controller.prototype.sendNotification = function(notification, properties) {
    var xhr = new XMLHttpRequest();
    var location = "http://" + this.hostName + "/events-control/" + this.pairingId;
    var query = notification + "=" + "%7B%22applianceId%22%3A%22" + this.applianceId + "%22%2C" + properties + "%7D";
    xhr.open("GET", location + "?" + query, false);
    xhr.send();
};


Controller.prototype.dpad = function() {
    this.sendNotification("webkitgamepadconnected", encodeURIComponent('"gamepad":{"id":"cloudtv dpad control"}'));
    var self = this;

    var touchGesture = function() {
        function getGestureIndex(gestures) {
            var max = Math.max.apply(null, gestures);
            if (max > 50) {
                return gestures.indexOf(max);
            }
        }

        var index = getGestureIndex([downX - upX, upX - downX, upY - downY, downY - upY]);
        if (index === 0) {
            console.log("15")
            self.sendNotification("webkitgamepadbuttondown", "%22button%22%3A%2215%22");
        }
        else if (index === 1) {
            console.log("14")
            self.sendNotification("webkitgamepadbuttondown", "%22button%22%3A%2214%22");
        }
        else if (index === 2) {
            console.log("12")
            self.sendNotification("webkitgamepadbuttondown", "%22button%22%3A%2212%22");
        }
        else if (index === 3) {
            console.log("13")
            self.sendNotification("webkitgamepadbuttondown", "%22button%22%3A%2213%22");
        }
    };

    var downX, upX, downY, upY;
    
    
    this.view.dpadSection.addEventListener("mousedown", function onTouchUnrelatedList(e) {
        e.preventDefault();
        downX = e.pageX;
        downY = e.pageY;
    });

    this.view.dpadSection.addEventListener("mouseup", function onTouchUnrelatedList(e) {
        upX = e.pageX;
        upY = e.pageY;
        touchGesture();
    });

    this.view.dpadSection.addEventListener("touchstart", function onTouchUnrelatedList(e) {
        downX = e.touches[0].pageX;
        downY = e.touches[0].pageY;
    });

    this.view.dpadSection.addEventListener("touchmove", function onTouchUnrelatedList(e) {
        e.preventDefault();
        upX = e.touches[0].pageX;
        upY = e.touches[0].pageY;
    });

    this.view.dpadSection.addEventListener("touchend", function onTouchUnrelatedList(e) {
        e.preventDefault()
        touchGesture();
    });
};

window.addEventListener("load", function() {
    new Controller();
},false);