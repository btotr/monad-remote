function SoundPairing(callback) {
    var self = this;
    self.callback = callback;
    navigator.webkitGetUserMedia({
        audio: true
    }, this.listen.bind(self));

}

SoundPairing.prototype.listen = function(stream) {

    // audio contect
    var audioContext = new webkitAudioContext();
    var mediaStreamSource = audioContext.createMediaStreamSource(stream);

    // filter
    var filter = audioContext.createBiquadFilter();
    filter.type = filter.HIGHPASS;
    filter.Q = 0;
    filter.frequency.value = 16000;
    mediaStreamSource.connect(filter);

    // analyser
    var analyser = audioContext.createAnalyser();
    analyser.fftSize = 2048;
    analyser.smoothingTimeConstant = 0;
    filter.connect(analyser);

    var frequencies = new Uint8Array(analyser.frequencyBinCount);

    // for (var i=0;i<1024;i++) console.log(i, i*audioContext.sampleRate/analyser.fftSize)
    var frequenciesMap = [
        789, // 17000
        799, // 17205
        808, // 17398
        818, // 17614
        827, // 17807
        836, // 18001
        846, // 18217
        855, // 18410
        864, // 18604
        873, // 18798
        883, // 19013
        892, // 19207
        901, // 19401
        910, // 19595
        920, // 19810
        929 // 20004
    ];
    
    var self = this;

    function getPairingId(original) {
        var items = [];
        var pairingid = [];
        var copy = original.slice(0);
        for (var i = 0; i < original.length; i++) {
            var count = 0;
            for (var w = 0; w < copy.length; w++) {
                if (original[i] == copy[w]) {
                    count++;
                    delete copy[w];
                }
            }
            if (count > 0) {
                items.push([original[i],count]);
            }
        }
        
        if (items.length < 4) {
            return false;
        }
        
        items.sort(function(a, b) {return b[1] - a[1]});
        for (var i=0;i<4;i++){
            pairingid.push(items[i][0]);
        }
        
        return pairingid.sort();
    };

    var tmpCode = [];

    function listenToPairingCode() {
        console.log("listenToPairingCode");
        for (var index in frequenciesMap) {
            analyser.getByteFrequencyData(frequencies);

            if (frequencies[frequenciesMap[index]] > 0) {
                tmpCode.push(frequenciesMap[index]);
            }
        }
        if (tmpCode.length < 300) {
            window.webkitRequestAnimationFrame(listenToPairingCode);
        }
        else {
            var pairingid = getPairingId(tmpCode);
            if (!pairingid) {
                window.webkitRequestAnimationFrame(listenToPairingCode);
                return;
            }
            self.callback(pairingid.join().replace(/,/g, ""));
        }
    }

    var counter = 0;

    function listenToStartFrequency() {
        analyser.getByteFrequencyData(frequencies);
        if (frequencies[frequenciesMap[0]] > 0) {
            console.log("start frequency found");
            if (counter == 5) {
                listenToPairingCode();
                return;
            }
            else {
                counter++;
                window.webkitRequestAnimationFrame(listenToStartFrequency);
                return;
            }
        }
        counter = 0;
        window.webkitRequestAnimationFrame(listenToStartFrequency);
    }

    listenToStartFrequency();
};
