var express = require('express');
var app = express();
var oneDay = 86400000;
var ip = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1',
    port = process.env.OPENSHIFT_NODEJS_PORT || '8080';
    
app.use(express.compress());
app.use(express.static(__dirname, { maxAge: oneDay }));
app.listen(port, ip);
console.log('Server running at http://'+ip+':'+port+'/');

